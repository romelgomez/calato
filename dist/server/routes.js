'use strict';
exports.__esModule = true;
// normalize the paths : http://stackoverflow.com/questions/9756567/do-you-need-to-use-path-join-in-node-js
var path = require("path");
// to adequate Angular build /dist/project_name/files...
var project_name_1 = require("./project-name");
var nodemailer = require('nodemailer');
var Q = require('q');
function sendEmail(emailOptions) {
    var deferred = Q.defer();
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            type: 'OAuth2',
            user: 'calatolima@gmail.com',
            clientId: '462272583161-11ebup35goceut82g7jf7boon6btl503.apps.googleusercontent.com',
            clientSecret: 'Ot6aa3I8DciAPFMP89Mzl8tM',
            refreshToken: '1/BWhBvLquuNkM_MqVboavU54_nKfI5K_T5pTplKSPeBM'
        }
    });
    transporter.sendMail(emailOptions, function (err, res) {
        if (err) {
            deferred.reject(err);
        }
        else {
            deferred.resolve(res);
        }
    });
    return deferred.promise;
}
var Routes = /** @class */ (function () {
    function Routes() {
    }
    Routes.prototype.defaultRoute = function (req, res) {
        res.sendFile('index.html', {
            root: path.join(process.cwd(), 'dist', project_name_1.projectName)
        });
    };
    Routes.prototype.paths = function (app) {
        var _this = this;
        app.get('/', function (req, res) {
            _this.defaultRoute(req, res);
        });
        app.get('/subscribe', function (req, res) {
            _this.defaultRoute(req, res);
        });
        app.get('/terms-and-conditions', function (req, res) {
            _this.defaultRoute(req, res);
        });
        app.get('/oops', function (req, res) {
            _this.defaultRoute(req, res);
        });
        app.post('/api/send-email', function (req, res) {
            // console.log('req.body', req.body);
            // console.log('api');
            var subscription = req.body;
            // res.redirect('/muchas-gracias');
            // res.redirect('/oops');
            var sex = '';
            switch (subscription['sex']) {
                case 'female':
                    sex = 'Femenino';
                    break;
                case 'male':
                    sex = 'Masculino';
                    break;
                default:
                    sex = 'Reservado';
            }
            /*
              {
                "name":"Romel Javier Gomez Herrera",
                "age":21,
                "sex":"male",
                "email":"bmxquiksilver7185@gmail.com",
                "phone":"4129427966",
                "veteran":"no",
                "referred":"si",
                "suggestion":"bmxquiksilver7185@gmail.com"
              }
            */
            sendEmail({
                from: 'CALATO',
                to: 'calatolima@gmail.com',
                subject: 'CALATO - ¡Nueva solicitud de suscripción!',
                text: "\n          Hola, un nuevo cliente quiere unirse a calato! \n\n\n\n          Sus datos son: \n\n          ---------------------------------------------- \n\n          Nombre: " + subscription['name'] + " \n\n          Edad: " + subscription['age'] + " \n\n          Sexo: " + sex + " \n\n          Correo: " + subscription['email'] + " \n\n          Tel\u00E9fono: " + subscription['phone'] + " \n\n\n\n          Preguntas: \n\n          ----------------------------------------------\n\n          \u00BFAlguna vez has participado en una reuni\u00F3n de este tipo?\n\n          " + subscription['veteran'] + "\n\n          \u00BFA trav\u00E9s de qu\u00E9 medio llegaste a nuestra p\u00E1gina?\n\n          " + subscription['referred'] + "\n\n          Te gustar\u00EDa que se realice una tem\u00E1tica en espec\u00EDfico. D\u00E9janos tu sugerencia...\n\n          " + subscription['suggestion'] + "\n        ",
                html: "\n          <h2>Hola, un nuevo cliente quiere unirse a calato!</h2>\n\n          <h3>Sus datos son:</h3>\n          ---------------------------------------------- <br>\n          <b>Nombre:</b> " + subscription['name'] + " <br>\n          <b>Edad:</b> " + subscription['age'] + " <br>\n          <b>Sexo:</b> " + sex + " <br>\n          <b>Correo:</b> " + subscription['email'] + " <br>\n          <b>Tel\u00E9fono:</b> " + subscription['phone'] + " <br><br>\n\n          <h3>Preguntas:</h3>\n          ----------------------------------------------<br>\n          <b>\u00BFAlguna vez has participado en una reuni\u00F3n de este tipo?</b><br>\n          " + subscription['veteran'] + "<br><br>\n          <b>\u00BFA trav\u00E9s de qu\u00E9 medio llegaste a nuestra p\u00E1gina?</b><br>\n          " + subscription['referred'] + "<br><br>\n          <b>Te gustar\u00EDa que se realice una tem\u00E1tica en espec\u00EDfico. D\u00E9janos tu sugerencia...</b><br>\n          " + subscription['suggestion'] + "\n        "
            })
                .then(function (response) {
                res.send({
                    result: 'ok'
                });
            }, function () {
                res.send({
                    result: 'error'
                });
            });
        });
        app.post('/api/test', function (req, res) {
            //   {
            //     accepted:[
            //        'calatolima@gmail.com'
            //     ],
            //     rejected:[
            //     ],
            //     envelopeTime:700,
            //     messageTime:1118,
            //     messageSize:586,
            //     response:'250 2.0.0 OK 1533260127 r138-v6sm910652vke.35 - gsmtp',
            //     envelope:{
            //        from:'',
            //        to:[
            //           Array
            //        ]
            //     },
            //     messageId:'<3b74c1b6-b4c7-87aa-1216-9674750b16dd@romel>'
            //  }
            sendEmail({
                from: 'CALATO',
                to: 'calatolima@gmail.com',
                subject: 'CALATO - ¡Nueva solicitud de suscripción!',
                text: 'Hola De nuevo... \n esta es otra linea'
            })
                .then(function (response) {
                res.send({
                    result: 'ok'
                });
            }, function () {
                res.send({
                    result: 'error'
                });
            });
        });
        app.get('test', function (req, res) {
            res.sendFile('runtime.a66f828dca56eeb90e02.js', {
                root: path.join(process.cwd(), 'dist', project_name_1.projectName)
            });
        });
        app.get('*', function (req, res) {
            _this.defaultRoute(req, res);
        });
    };
    return Routes;
}());
exports.Routes = Routes;
