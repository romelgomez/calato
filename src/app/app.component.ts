import { Component, OnInit, AfterViewInit } from '@angular/core';
declare var jquery: any;
declare var $: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {

  ngOnInit() {
  }

  ngAfterViewInit() {
    $('app-root')
      .bind('DOMSubtreeModified', function() {
        if (window.location.pathname === '/subscribe') {
          $('body')
            .addClass('subscribe');
        } else {
          $('body')
            .removeClass('subscribe');
        }
        if ( window.location.pathname === '/terms-and-conditions' ) {
          $('html, body').animate({scrollTop: '0px'}, 0, 'linear');
        }
      });
  }

}
