import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgxSpinnerModule } from 'ngx-spinner';
import { SnotifyModule, SnotifyService, ToastDefaults } from 'ng-snotify';

import { CalatoApiService } from './services/calato-api.service';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SubscribeComponent } from './subscribe/subscribe.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions.component';
import { OopsComponent } from './oops/oops.component';

const appRoutes: Routes = [
  { path: 'home',                       component: HomeComponent },
  { path: 'subscribe',                  component: SubscribeComponent },
  { path: 'terms-and-conditions',       component: TermsAndConditionsComponent },
  { path: 'oops',                       component: OopsComponent },
  { path: '',                           component: HomeComponent },
  { path: '**',                         component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SubscribeComponent,
    PageNotFoundComponent,
    TermsAndConditionsComponent,
    OopsComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      appRoutes,
    ),
    ReactiveFormsModule,
    HttpClientModule,
    NgxSpinnerModule,
    SnotifyModule
  ],
  providers: [
    CalatoApiService,
    { provide: 'SnotifyToastConfig', useValue: ToastDefaults},
    SnotifyService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
