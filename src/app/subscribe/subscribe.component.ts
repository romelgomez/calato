import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidationErrors, AbstractControl} from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { CalatoApiService } from '../services/calato-api.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { SnotifyService } from 'ng-snotify';


@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: ['./subscribe.component.scss'],
  providers: [ CalatoApiService, NgxSpinnerService, SnotifyService ],
})
export class SubscribeComponent implements OnInit {

  public subscribeForm: FormGroup;

  constructor(private calatoApi: CalatoApiService,
              private spinner: NgxSpinnerService,
              private snotifyService: SnotifyService ) {}



  ngOnInit() {

    this.subscribeForm = new FormGroup ({
      name: new FormControl('', [
        Validators.required,
      ]),
      age: new FormControl('', [
        Validators.required,
      ]),
      sex: new FormControl('female', [
        Validators.required,
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.email,
      ]),
      phone: new FormControl('', [
        Validators.required,
      ]),
      veteran: new FormControl('', [
        Validators.required,
      ]),
      referred: new FormControl('Facebook', [
        Validators.required,
      ]),
      suggestion: new FormControl('', []),
    });

  }

  inputInstance ( inputName: string ) {
    return this.subscribeForm.get(inputName);
  }

  errorMessages ( inputName: string ) {

    const inputInstance = this.subscribeForm.get( inputName );
    let error: string;

    switch ( inputName ) {
      case 'name':
        error = inputInstance
                  .hasError('required') ? 'El nombre es obligatorio.' : '';
        break;
      case 'age':
        error = inputInstance
                  .hasError('required') ? 'La edad es obligatorio.' : '';
        break;
      case 'sex':
        error = inputInstance
                  .hasError('required') ? 'Seleccionar una opción es obligatorio.' : '';
        break;
      case 'email':
        error = inputInstance
                  .hasError('required') ? 'El email es obligatorio.' :
                inputInstance
                  .hasError('email') ? 'Debe ser un email válido' : '';
        break;
      case 'phone':
        error = inputInstance
                  .hasError('required') ? 'El telefono es obligatorio.' : '';
        break;
      case 'veteran':
        error = inputInstance
                  .hasError('required') ? 'Seleccionar una opción es obligatorio.' : '';
        break;
      case 'referred':
        error = inputInstance
                .hasError('required') ? 'Es importante que describas de donde eres referido.' : '';
        break;
    }

    return error;
  }

  success () {
  }

  failed (error) {
  }

  test () {
    console.log('Test funcion in the component is called...');
    this.calatoApi.test()
      .subscribe ( console.log, console.error );
  }

  testsNotifyService () {
    // this.snotifyService.success('Su solicitud se envió exitosamente!', 'Super!', {
    //   position: 'centerCenter'
    // });

    this.snotifyService.error('Hubo un problema al procesar su solicitud!', 'Oops!', {
      position: 'centerCenter'
    });
  }

  onSubmit() {
    this.spinner.show();
    this.calatoApi.sendEmail( this.subscribeForm.value )
      .subscribe ( send => {
        this.spinner.hide();
        if ( send['result'] === 'ok') {
          this.snotifyService.success('Su solicitud se envió exitosamente!', 'Super!', {
            position: 'centerCenter'
          });
          this.subscribeForm.reset();
        } else {
          this.snotifyService.error('Hubo un problema al procesar su solicitud!', 'Oops!', {
            position: 'centerCenter'
          });
        }

      }, function () {
        this.snotifyService.error('Hubo un problema al procesar su solicitud!', 'Oops!', {
          position: 'centerCenter'
        });
      });

  }


}
