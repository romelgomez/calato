import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class CalatoApiService {

  //  private API_URL = 'https://wl.mapsalud.com/mapsalud/services/public';

  constructor(private http: HttpClient) { }

  sendEmail ( formData: Object ) {
    return this.http.post( '/api/send-email', formData);
  }

  test () {
    return this.http.post( '/api/test', {});
  }

}
